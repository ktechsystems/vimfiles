#!/usr/bin/env bash

# Update plugins under the bundle dir.
pushd .
cd bundle/

for D in *;
    do [ -d "${D}" ] && cd ${D} && git pull && cd ..;
done

popd

