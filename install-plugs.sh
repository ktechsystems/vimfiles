#!/usr/bin/env bash

# Create bundle dir if need be
if [ ! -d bundle ]; then
    mkdir bundle
fi

# Install plugins under the bundle dir.
#cd bundle/

function clone() {
    cd bundle/
    if [ ! -d $1 ]; then
        git clone $2
    fi
    cd ..
}

function unzip_from() {
    cd bundle/
    if [ ! -d $1 ]; then
        mkdir $1
        curl -L -o /tmp/$1 $2
        unzip /tmp/$1 -d ./$1
    fi
    cd ..
}


# Install exuberant ctags
echo Install exuberant ctags for use with vim-taglist using
echo sudo apt-get install exuberant-ctags


# Install plugins from downloaded zips

unzip_from vim-taglist http://sourceforge.net/projects/vim-taglist/files/latest/download


# Install plugins from github via clone

clone vim-snipmate https://github.com/garbas/vim-snipmate.git
clone tlib_vim https://github.com/tomtom/tlib_vim.git
clone vim-addon-mw-utils https://github.com/MarcWeber/vim-addon-mw-utils.git
clone snipmate-snippets https://github.com/jmcaffee/snipmate-snippets.git
clone sessionman.vim https://github.com/vim-scripts/sessionman.vim.git
clone vim-ruby https://github.com/vim-ruby/vim-ruby.git
clone vim-ruby-debugger https://github.com/astashov/vim-ruby-debugger.git
clone Rename2 https://github.com/vim-scripts/Rename2.git
clone vim-hilinks https://github.com/kergoth/vim-hilinks.git
clone nerdtree https://github.com/scrooloose/nerdtree.git
clone VimRegEx.vim https://github.com/vim-scripts/VimRegEx.vim.git
clone vsutil.vim https://github.com/vim-scripts/vsutil.vim.git
clone ag.vim https://github.com/rking/ag.vim.git
clone vim-colors-solarized https://github.com/altercation/vim-colors-solarized.git
clone vim-endwise https://github.com/tpope/vim-endwise.git
clone vim-eunuch https://github.com/tpope/vim-eunuch.git
clone vim-go https://github.com/fatih/vim-go.git
clone delimitMate https://github.com/Raimondi/delimitMate.git
clone vim-elixir https://github.com/elixir-lang/vim-elixir.git
clone vim-mix https://github.com/mattreduce/vim-mix.git
clone vim-crystal https://github.com/rhysd/vim-crystal.git
clone vim-surround https://github.com/tpope/vim-surround.git
clone vim-javascript https://github.com/pangloss/vim-javascript.git
#cd ..

echo Plugins cloned...
echo Running vim to generate HelpTags
echo
echo You can exit vim after it has run.

vim +Helptags

