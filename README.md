        VIMFILES
=============================

Customized vim configuration files.

Install
-----------------------------

Pull the repo down:

    $ cd ~/
    $ git clone https://ktechsystems@bitbucket.org/ktechsystems/vimfiles.git vimfiles

Setup
-----------------------------
Copy and rename _vimrc.example to remove the file extension (.example).
Place the copy into the root of your home directory.
Filename after copy/rename:

    Windows: _vimrc
    Unix: .vimrc

    On Windows 7, your home directory is located at \Windows\Users\YOUR_USER_NAME, so:
        \Windows\Users\Somename\_vimrc

    On Linux, your home directory is located at /home/YOUR_USER_NAME, so:
        /home/Somename/.vimrc

The vimfiles folder should also be placed in your home directory:

    ~/vimfiles

If you are running a windows system, edit the MySys function so that it returns "windows".

### Install Plugins

Change into the `vimfiles` directory and run the plugin install script

    $ cd vimfiles
    $ ./install-plugs.sh

All plugins will be `git clone`'d into the `bundle` directory and vim will be
started with the `+Helptags` option to generate plugin documentation.

You can close vim: `:q<enter>`

To update the plugins

    $ ./update-plugs.sh


#### vim-go

One of the plugins installed is vim-go. If you've set your go environment up,
you can run `:GoInstallBinaries` inside of vim to install additional tools
related to go-lang. See [vim-go](https://github.com/fatih/vim-go) for further details.

#### vim-ruby-debugger Plugin

vim-ruby-debugger requires the debugger-xml gem and only runs on
Ruby >= 1.9. The gem can be installed with

    gem install debugger-xml

It does **not** run on windows at this time. See [vim-ruby-debugger](https://github.com/astashov/vim-ruby-debugger)
for more information.

#### Help tags

See `:help helptags` for infomation on rebuilding help tags.

