" File: guideline.vim
" Author: Jeff McAffee
" Description: GDL Syntax Definition file
" Last Modified: 10/14/2011
"
if exists("b:current_syntax")
    " echo "Exiting guideline.vim... current_syntax already set."
    finish
endif

" echo "Running guideline.vim"

" Common keywords {
  syn keyword gdlEnd end
 
" }

" Special Operators {
  syn match gdlAssignmentOperator  "[^=]\([=]\)[^=]"
" }

" Conditional Operators {
  "syn match gdlCondComparisonOperators "\([=]\)\1"
  syn match gdlCondComparisonOperators "[^=]\(==\)[^=]"
  syn match gdlCondComparisonOperators "!="
  syn match gdlCondComparisonOperators "<"
  syn match gdlCondComparisonOperators ">"
  syn match gdlCondComparisonOperators "<="
  syn match gdlCondComparisonOperators ">="
  syn match gdlCondComparisonOperators "<>"
  syn match gdlCondComparisonOperators "??"
  syn keyword gdlCondComparisonOperators in

  syn match gdlCondOperators "&&"
  syn match gdlCondOperators "||"
  syn match gdlCondOperators " and "
  syn match gdlCondOperators " or "
" }


  syn keyword gdlObjects rule ruleset guideline


  syn region gdlString start=+L\="+ skip=+\\\\\|\\"\|\\$+ excludenl end=+"+ end='$'
  syn match gdlDeReference '()' contained
  syn keyword gdlLineTerminator ;

  syn keyword gdlFlow if then else end

  syn match gdlConstantNum "\d\+"
 
" Variable Definitions {
  syn keyword gdlVarDefTypes decision ppm dpm dsm nextgroup=gdlVarTypes skipwhite contained

  syn keyword gdlVarTypes boolean text money percentage date datetime nextgroup=gdlVarDefIdentifier skipwhite
  syn keyword gdlVarTypes numeric nextgroup=gdlVarPrecision, gdlVarDefIdentifier skipwhite
  syn match gdlVarPrecision '(\d\+)' nextgroup=gdlVarDefIdentifier skipwhite

  syn region gdlVarDefRegion start="ppm" start="dpm" start="dsm" start="decision" end=";" end="$" transparent keepend contains=gdlVarDefTypes,gdlVarTypes,gdlVarPrecision,gdlVarDefIdentifier,gdlVarDefAlias
  syn match gdlVarDefIdentifier '[-[:alnum:]_]\+' nextgroup=gdlVarDefAlias contained skipwhite
  syn match gdlVarDefAlias +"[^"]*"+ contained
" }

  "syn match gdlVarIdentifier '[-[:alnum:]_]\+'

" Rule Reference {
  syn keyword gdlRuleKeyword rule nextgroup=gdlRuleIdentifier skipwhite contained
  syn match gdlRuleIdentifier '[-[:alnum:]_]\+' nextgroup=gdlDeReference contained
  syn region gdlRuleRefRegion start="rule" end=")" contains=gdlRuleKeyword,gdlRuleIdentifier oneline
" }

" Ruleset Reference {
  syn keyword gdlRulesetKeyword ruleset nextgroup=gdlRulesetIdentifier skipwhite contained
  syn match gdlRulesetIdentifier '[-[:alnum:]_]\+' nextgroup=gdlDeReference contained
  syn region gdlRulesetRefRegion start="ruleset" end=")" contains=gdlRulesetKeyword,gdlRulesetIdentifier oneline
" }

" PreProcessor {
  syn keyword gdlPreProc include nextgroup=gdlIncFileRef contained
  syn match gdlIncFileRef #("[-./[:alnum:]_~]\+")# nextgroup=gdlLineTerminator contained
  syn region gdlIncFileRegion start="include" end=";" oneline transparent contains=gdlPreProc,gdlIncFileRef,gdlLineTerminator

  syn keyword gdlPreProc import nextgroup=gdlImpFileRef contained
  syn match gdlImpFileRef #([:alnum:], "[-./[:alnum:]_~]\+")# nextgroup=gdlLineTerminator contained
  syn region gdlImpFileRegion start="import" end=";" oneline transparent contains=gdlPreProc,gdlImpFileRef,gdlLineTerminator

" }

  syn match gdlMathOperators "+"
  syn match gdlMathOperators " \- "
  syn match gdlMathOperators " \* "
  syn match gdlMathOperators "/"
  syn match gdlMathOperators "\^"

  syn region gdlCommentLine start="//" skip="\\$" end="$"
  syn region gdlComment start="/\*" end="\*/"


" Turn on highlighting {
  let b:current_syntax = "guideline"

  hi def link gdlVarDefTypes            Keyword
  hi def link gdlRulesetKeyword         Keyword
  hi def link gdlEnd                    Keyword

  hi def link gdlRuleKeyword            Define
  hi def link gdlRuleIdentifier         Define
  " Override gdlObjects (ruleset, rule, guideline. etc.)
  hi def link gdlObjects                Define

  hi def link gdlRulesetIdentifier      Function

  hi def link gdlFlow                   Conditional

  hi def link gdlVarTypes               Type
  hi def link gdlConstantNum            Number
  hi def link gdlVarPrecision           Special
  hi def link gdlVarDefAlias            Special

  " Don't hilight the assigment operator
  "hi def link gdlAssignmentOperator Operator
  hi def link gdlCondComparisonOperators Operator
  hi def link gdlCondOperators          Operator
  hi def link gdlMathOperators          Operator

  hi def link gdlVarDefIdentifier       Identifier


  hi def link gdlPreProc                Include
" Define is yellow
"  hi def link gdlIncFileRef Define

  hi def link gdlString                 String

  hi def link gdlCommentLine            Comment
  hi def link gdlComment                Comment

" }


"" Turn on highlighting {
"  let b:current_syntax = "guideline"
"
"  hi def link gdlVarDefTypes Keyword
"  hi def link gdlVarTypes Type
"  hi def link gdlVarPrecision Special
"  hi def link gdlVarDefIdentifier Identifier
"  hi def link gdlVarDefAlias Special
"
"  "hi def link gdlRuleKeyword Keyword
"  "hi def link gdlRuleIdentifier Function
"  hi def link gdlRuleKeyword Define
"  hi def link gdlRuleIdentifier Define
"
"  hi def link gdlRulesetKeyword Keyword
"  hi def link gdlRulesetIdentifier Function
"
"  " Override gdlObjects (ruleset, rule, guideline. etc.)
"  hi def link gdlObjects Define
"  hi def link gdlEnd Keyword
"
"  hi def link gdlPreProc Include
"" Define is yellow
""  hi def link gdlIncFileRef Define
"
"  hi def link gdlCommentLine Comment
"  hi def link gdlComment Comment
"  hi def link gdlConstantNum Number
"
"  " Don't hilight the assigment operator
"  "hi def link gdlAssignmentOperator Operator
"
"  hi def link gdlCondComparisonOperators Operator
"  hi def link gdlCondOperators Operator
"  hi def link gdlMathOperators Operator
"  hi def link gdlString String
"  hi def link gdlFlow Conditional
"" }

