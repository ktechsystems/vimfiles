" File: guideline.vim
" Author: Jeff McAffee
" Description: GDL Syntax Definition file
" Last Modified: 10/14/2011
"
"if exists("b:current_syntax")
"    " echo "Exiting guideline.vim... current_syntax already set."
"    finish
"endif

" echo "Running guideline.vim"



" Special Operators {
  syn match gdlAssignmentOperator  "[^=]\([=]\)[^=]"
" }

" Conditional Operators {
  "syn match gdlCondComparisonOperators "\([=]\)\1"
  syn match gdlCondComparisonOperators "[^=]\(==\)[^=]"
  syn match gdlCondComparisonOperators "!="
  syn match gdlCondComparisonOperators "<"
  syn match gdlCondComparisonOperators ">"
  syn match gdlCondComparisonOperators "<="
  syn match gdlCondComparisonOperators ">="
  syn match gdlCondComparisonOperators "<>"
  syn match gdlCondComparisonOperators "??"
  syn keyword gdlCondComparisonOperators in

  syn match gdlCondOperators "&&"
  syn match gdlCondOperators "||"
  syn match gdlCondOperators " and "
  syn match gdlCondOperators " or "
" }

" Math Operators {
  syn match gdlMathOperators "+"
  syn match gdlMathOperators " \- "
  syn match gdlMathOperators " \* "
  syn match gdlMathOperators "/"
  syn match gdlMathOperators "\^"
" }


  syn keyword gdlLineTerminator ;

  syn match gdlConstantNum "\d\+" contained

  syn keyword gdlConst NULL true false
 
" If Statement {
  "syn match gdlIf /if/ nextgroup=gdlIfCondition skipwhite
  "syn match gdlIfCondition /([^)]*)/ contained nextgroup=gdlThen skipnl skipwhite
  "syn match gdlThen /then/ contained

" }

"  syn match testIfCondition /([^)]*)/ skipnl skipwhite
"  syn region testIf start=/if[\s]*(/ end=/)/ contains=ALL
"  syn match testIfKey /if(/


  "syn match xIf /if\s*(/
  syn keyword gdlIf if
  syn keyword gdlThen then
  syn keyword gdlElse else
  syn keyword gdlEnd end transparent
  syn keyword gdlRule rule
  
  syn match gdlRuleDeref /()/ contained

  "syn match xIfBlock /if[^end]/
  "syn match xIfBlock /if.*/ contains=xIf
  "syn match xIfBlock /if.*/ contains=xIf,xOp

  syn match xOp /&&/ contained
  "syn match xOp /&&/

  "syn match gdlRuleId /rule\s+.+\s*()/ contains=gdlIdentifier
  syn region xIfReg matchgroup=gdlKeyword start=/if/ end=/end/ contains=xExpReg,gdlCondOperators keepend
  syn region xExpReg start=/(/ms=e+1 end=/)/me=s-1 contained contains=xExpReg,gdlVarIdent,gdlCondComparisonOperators,gdlStrConst,gdlCondOperators
  syn match gdlStrConst /"[^"]"/
  syn match gdlVarIdent '[-[:alnum:]_]\+' contained transparent

  "syn region gdlRuleRegion matchgroup=gdlObjects start=/rule/ end=/end/ keepend nextgroup=gdlRuleId skipwhite transparent contains=gdlIdentifier,gdlRuleDeref,xIfReg
  "syn match gdlIdentifier /[-[:alnum:]_]\+/ contained nextgroup=gdlRuleDeref skipwhite


  syn match xRule /^rule.*/ contains=xId,xParens nextgroup=xId skipwhite
  syn match xId /[-[:alnum:]_]\+/ contained nextgroup=xParens
  syn match xParens /()/ contained

  hi! def link xRule Error
  hi! def link xId Structure
"
" Turn on highlighting {
  let b:current_syntax = "guideline"

  hi! def link gdlIf gdlKeyword
  hi! def link gdlThen gdlKeyword
  hi! def link gdlElse gdlKeyword
  hi! def link gdlCondOperators gdlOperators
  hi! def link gdlCondComparisonOperators gdlOperators

  hi! def link gdlConst gdlSpecial

  hi! def link gdlIdentifier gdlFunction

  hi! def link gdlRule gdlObjects
  hi! def link gdlObjects Define

  hi! def link gdlKeyword      Keyword
  hi! def link xIfBlock Define
  hi! def link xOp      Operator
  "hi! def link xIfReg   Include
  hi! def link gdlStrConst  String
  hi! def link gdlVarIdent  Identifier
  hi! def link gdlOperators Structure
  hi! def link gdlSpecial Special
  hi! def link gdlFunction Error
  "hi! def link gdlIdentifier  Identifier
  "hi! def link xExpReg  NONE

" }

