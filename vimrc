" Ruby Debugger Settings {
  let g:ruby_debugger_spec_path = '/opt/bin/Ruby193/bin/rspec' " set Rspec path
  let g:ruby_debugger_cucumber_path = 'cucumber' " set Cucumber path
  " set path to start server. Note, this is relative to vim working dir. use
  " :cd to set to rails project root before calling.
  let g:ruby_debugger_default_script = 'script/rails s'
" }

let mysys=MySys()

":gui
" Modeline and Notes {
"   vim: set foldmarker={,}  foldlevel=0 spell:
"
"   This _vimrc is based on one found at http://vi-improved.org/vimrc.html
" }

    " Turn off spell checking {
      let loaded_spellfile_plugin=1
    " }

" Pathogen initialization {
"   See https://github.com/tpope/vim-pathogen
  call pathogen#infect("~/vimfiles/bundle")
"   }

" Basics {
    set nocompatible " explicitly get out of vi-compatible mode 
    set noexrc " don't use local version of _(g)vimrc, _exrc
    set background=dark " we plan to use a dark background
    set cpoptions=aABceFsmq
    "             |||||||||
    "             ||||||||+-- When joining lines, leave the cursor
    "             ||||||||     between joined lines
    "             |||||||+-- When a new match is created (showmatch)
    "             |||||||      pause for .5
    "             ||||||+-- Set buffer options when entering the buffer
    "             |||||+-- :write command updates current file name
    "             ||||+-- Automatically add <CR> to the last line
    "             ||||     when using :@r
    "             |||+-- Searching continues at the end of the match
    "             |||     at the cursor position
    "             ||+-- A backslash has no special meaning in mappings
    "             |+-- :write updates alternative file name
    "             +-- :read updates alternative file name
    syntax on " syntax highlighting on
    if hostname() ==? 'ares'
        if mysys ==? "windows"
            let font = {"name" : "Consolas", "size" : "h10"}
        else
            set t_Co=256
            colorscheme solarized
            let font = {"name" : "Consolas", "size" : "12"}
        endif
    else
        if mysys ==? "windows"
            let font = {"name" : "Consolas", "size" : "h10"}
        else
            set t_Co=256
            colorscheme myslate
            let font = {"name" : "Inconsolata", "size" : "12"}
        endif
    endif
" }

" General {
    filetype plugin indent on " load filetype plugins/indent settings
    set autochdir " always switch to the current file directory
    set backspace=indent,eol,start " make backspace a more flexible
    set nobackup " don't make backups
    " set backupdir=$VIM\backup " where to put backup files
    set clipboard+=unnamed " share windows clipboard
    set noswapfile " turn off swapfiles - I never use them
    "set directory=~/vimfiles/tmp " directory to place swap files in
if mysys == "windows"
    set fileformats=dos,unix,mac " support all three, in this order
else
    set fileformats=unix,dos,mac " support all three, in this order
    set shell=/bin/bash " force bash as shell
endif
    set hidden " you can change buffers without saving
    " (XXX: #VIM/tpope warns the line below could break things)
    set iskeyword+=_,$,@,%,# " none of these are word dividers
    set mouse=a " use mouse everywhere
    set noerrorbells " don't make noise
    set whichwrap=b,s,h,l,<,>,~,[,] " everything wraps
    "             | | | | | | | | |
    "             | | | | | | | | +-- "]" Insert and Replace
    "             | | | | | | | +-- "[" Insert and Replace
    "             | | | | | | +-- "~" Normal
    "             | | | | | +-- <Right> Normal and Visual
    "             | | | | +-- <Left> Normal and Visual
    "             | | | +-- "l" Normal and Visual (not recommended)
    "             | | +-- "h" Normal and Visual (not recommended)
    "             | +-- " <Space> Normal and Visual
    "             +-- "<BS> Normal and Visual
    set wildmenu " turn on command completion wild style
    " ignore these list file extensions
    set wildignore=*.dll,*.o,*.obj,*.bak,*.exe,*.pyc,*.jpg,*.gif,*.png,*.psd,*.sqlite3
    set wildmode=longest:list " turn on wild mode huge list
    " Define what sessions will store
    set sessionoptions=curdir,buffers,tabpages
" }

" Vim UI {
    set nocursorcolumn " don't highlight the current column
    set cursorline " highlight the current line
    set incsearch " BUT do highlight as you type your search phrase
    set laststatus=2 " always show the status line
    set lazyredraw " do not redraw while running macros
    set linespace=0 " don't insert any extra pixel lines between rows
    set list " we do want to show tabs, so we can get rid of them
    set listchars=tab:>-,trail:- " show tabs and trailing
    set matchtime=5 " how many tenths of second to blink matching brackets
    set hlsearch " highlight searched for phrases
    set nostartofline " leave my cursor where it was
    set visualbell " blink, don't ring the f'n bell!
    set number " turn on line numbers
    set numberwidth=5 " We are good up to 99999 lines
    set report=0 " tell use when anything is changed via :...
    set ruler " Always show current positions along the bottom
    set scrolloff=10 " Keep 10 line (top/bottom) for scope
    set shortmess=aOstT " shortens messages to avoid 'press a key' prompt
    set showcmd " show the command being typed
    set showmatch " show matching brackets
    set sidescrolloff=10 " Keep 5 lines at the side
    let g:screen_size_restore_pos = 1 " Vim will remember its size and position when re-opened
    "set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]%{ruby_debugger#statusline()}
    set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][L:%04l,C:%04v]
    "              | | | | |  |   |      |  |     |      |       |
    "              | | | | |  |   |      |  |     |      |       +-- ruby debugger
    "              | | | | |  |   |      |  |     |      +-- current col
    "              | | | | |  |   |      |  |     +-- current line
    "              | | | | |  |   |      |  +-- current % into file
    "              | | | | |  |   |      +-- current syntax in brackets
    "              | | | | |  |   +-- current fileformat
    "              | | | | |  +-- number of lines
    "              | | | | +-- preview flag in square brackets
    "              | | | +-- help flag in square brackets
    "              | | +-- readonly flag in square brackets
    "              | +-- modified flag in square brackets
    "              +-- full path to file in the buffer
" }

" Text Formatting/Layout {
    "set completeopt= " don't use a pop up menu for completions
    set completeopt=menu "use a pop up menu for completions
    set expandtab " no real tabs please!
    set formatoptions=rq " Automatically insert comment leader on return,
                         " and let gq format comments
    set ignorecase " case insensitive by default
    set infercase " case inferred by default
    set nowrap " do not wrap line
    set shiftround " when at 3 spaces, and I hit > ... go to 4, not 5
    set smartcase " if there are caps, go case-sensitive
    set shiftwidth=4 " auto-indent amount when using cindent
                     " >>, << and stuff like that
    set softtabstop=4 " when hitting tab or backspace, how many spaces
                      " should a tab be (see expandtab)
    set tabstop=8 " real tabs should be 8, and they will show with
                  " set list on
" }

" Folding {
    set foldenable " turn on folding
    set foldmarker={,} " Fold C style code (only use this as default
                       " if you use a high foldlevel)
    set foldmethod=marker " fold on the marker
    set foldlevel=100 " Don't autofold anything (but I can still
                      " fold manually)
    set foldopen=block,hor,mark,percent,quickfix,tag " what movements
                                                     " open folds
        function SimpleFoldText() " {
            return getline(v:foldstart).' '
        endfunction " }

    set foldtext=SimpleFoldText() " Custom fold text function
                                  " (cleaner than default)
" }

" Plugin Settings {
    let b:match_ignorecase = 1 " case is stupid
    let perl_extended_vars = 1 " highlight advanced perl vars
                               " inside strings

    " TagList Settings {
        let Tlist_Auto_Open=0 " let the tag list open automagically
        let Tlist_Auto_Update = 1 " update the tag list when a new file is edited
        let Tlist_Close_On_Select = 1 " Automatically close the tag window on tag selection
        let Tlist_Compact_Format = 1 " show small menu
        let Tlist_Ctags_Cmd = 'ctags' " location of ctags
        let Tlist_Enable_Fold_Column = 0 " don't show folding tree
        let Tlist_Exist_OnlyWindow = 1 " if you are the last, kill
                                       " yourself
        let Tlist_File_Fold_Auto_Close = 0 " don't fold closed other trees
        let Tlist_GainFocus_On_ToggleOpen = 1 " move the cursor to the tag list when toggled open
        let Tlist_Sort_Type = "name" " order by
        let Tlist_Use_Right_Window = 1 " split to the right side
                                       " of the screen
        let Tlist_WinWidth = 40 " 40 cols wide, so I can (almost always)
                                " read my functions

        " Language Specifics {
            " just functions and classes please
            let tlist_aspjscript_settings = 'asp;f:function;c:class'
            " just functions and subs please
            let tlist_aspvbs_settings = 'asp;f:function;s:sub'
            " don't show variables in freaking php
            let tlist_php_settings = 'php;c:class;d:constant;f:function'
            " just functions and classes please
            let tlist_vb_settings = 'asp;f:function;c:class'
            " guideline tags
            let tlist_guideline_settings = 'Guideline;r:rule;s:ruleRef;p:ruleset;q:rulesetRef'
        " }
    " }

    " delimitMate Settings {
        " Put a blank line between delimiter expansions when hitting CR
        let g:delimitMate_expand_cr = 2
    " }
" }

" Mappings {
    " set the mapleader
    "let mapleader=","

    " ROT13 - fun
    map <F12> ggVGg?

    " space / shift-space scroll in normal mode
    noremap <S-space> <C-b>
    noremap <space> <C-f>

    " Alt-j and Alt-k move lines of text in visual and insert modes
    nnoremap <A-j> :m+<CR>==
    nnoremap <A-k> :m-2<CR>==
    inoremap <A-j> <Esc>:m+<CR>==gi
    inoremap <A-k> <Esc>:m-2<CR>==gi
    vnoremap <A-j> :m'>+<CR>gv=gv
    vnoremap <A-k> :m-2<CR>gv=gv

    " Make Arrow Keys Useful Again {
        map <down> <ESC>:bn<RETURN>
        map <left> <ESC>:NERDTreeToggle<RETURN>
        map <right> <ESC>:TlistToggle<RETURN>
        map <up> <ESC>:bp<RETURN>
    " }

    " Ctrl-Tab switches between tabs {
        map <C-Tab> <ESC>:tabn<RETURN>
        map <C-S-Tab> <ESC>:tabp<RETURN>
    " }

    " Syntax Debugging Helpers {
        map <F10> <ESC>:echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
        map <s-F10> <ESC>:HLT<CR>
    " }

    " Indent Helpers {
        " Retain selection in visual mode when indenting blocks
        vnoremap < <gv
        vnoremap > >gv
    " }

" }

" Commands (custom) {
  " Clear the search
  command Cls let @/ = ''
  " Turn on/off soft wrapping. Note that list (show tabs) and wrap are
  " incompatible.
  command! -nargs=* Wrap set wrap linebreak nolist
  command! -nargs=* Nowrap set nowrap list
  " Delete leading whitespace (but only if line is only whitespace)
  command! -nargs=* DelWs let _s=@/<Bar>:%s/^\s\+$//e<Bar>let @/=_s<Bar>:nohl
  " Delete trailing whitespace (do not run this on markdown files!)
  command! -nargs=* DelTws let _s=@/<Bar>:%s/\s\+$//e<Bar>let @/=_s<Bar>:nohl
  " Set syntax to guideline.gdl
  command! -nargs=* Syngdl set ft=guideline.gdl
  " Set syntax to shell
  command! -nargs=* Synbash set ft=sh.bash
  " Replace line endings with unix style line endings
  command! -nargs=* ToUnix update | e ++ff=dos | setlocal ff=unix
  " XML Tidy
  command! Thtml :%! tidy -q -i --show-errors 0
  command! Tthtml :%! tidy -q -i --indent-attributes y --show-errors 0
  command! Txml :%! tidy -q -i --show-errors 0 -xml
  command! Ttxml :%! tidy -q -i --indent-attributes y --show-errors 0 -xml
" }

" Autocommands/File Types {
    " Guideline {
        " gdl standard 2 spaces, always
        au BufNewFile,BufRead *.gdl setf guideline
        au BufRead,BufNewFile *.gdl set syntax=gdl

        au BufRead,BufNewFile *.gdl set shiftwidth=2
        au BufRead,BufNewFile *.gdl set softtabstop=2
    " }
    " Ruby {
        " ruby standard 2 spaces, always
        au BufRead,BufNewFile *.rb,*.rhtml,*.rgdl set shiftwidth=2
        au BufRead,BufNewFile *.rb,*.rhtml,*.rgdl set softtabstop=2
        au BufRead,BufNewFile *.rb,*.rhtml,*.rgdl set syntax=ruby
        au FileType ruby,eruby set omnifunc=rubycomplete#Complete
        au FileType ruby,eruby let g:rubycomplete_rails=1
        au FileType ruby,eruby let g:rubycomplete_buffer_loading=1
        au FileType ruby,eruby let g:rubycomplete_classes_in_global=1
    " }
    " Haml {
        " haml, sass standard 2 spaces, always
        au BufRead,BufNewFile *.haml,*.scss,*.sass set shiftwidth=2
        au BufRead,BufNewFile *.haml,*.scss,*.sass set softtabstop=2
    " }
    " Notes {
        " I consider .notes files special, and handle the differently,
        " I should probably put this in another file
        au BufRead,BufNewFile *.notes set foldlevel=2
        au BufRead,BufNewFile *.notes set foldmethod=indent
        au BufRead,BufNewFile *.notes set foldtext=foldtext()
        au BufRead,BufNewFile *.notes set listchars=tab:\ \
        au BufRead,BufNewFile *.notes set noexpandtab
        au BufRead,BufNewFile *.notes set shiftwidth=8
        au BufRead,BufNewFile *.notes set softtabstop=8
        au BufRead,BufNewFile *.notes set tabstop=8
        au BufRead,BufNewFile *.notes set syntax=notes
        au BufRead,BufNewFile *.notes set nocursorcolumn
        au BufRead,BufNewFile *.notes set nocursorline 
        au BufRead,BufNewFile *.notes set spell 
    " }
    " Go {
        " Go uses Tabs instead of spaces
        au BufNewFile,BufRead,BufWinEnter,BufEnter *.go set nolist
    " }
    " Markdown {
        "au! BufWinEnter,BufNewFile,BufRead *.md setfiletype=markdown
        "au! BufWinEnter,BufNewFile,BufRead *.md setf markdown
        "au BufRead,BufNewFile *.md set syntax=markdown
    " }
    " au BufNewFileBufRead *.ahk setf ahk " Group is not currently setup
    " Crystal {
        au FileType crystal setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2
    " }
    " JS/React {
        " standard 2 spaces, always
        au BufRead,BufNewFile *.js,*.jsx,*.ts,*.tsx set shiftwidth=2
        au BufRead,BufNewFile *.js,*.jsx,*.ts,*.tsx set softtabstop=2
        au BufRead,BufNewFile *.js,*.jsx,*.ts,*.tsx set syntax=javascript
    " }
" }

" GUI Settings {
if has("gui_running")
    " Basics {
        colorscheme myslate " color scheme I like
        "set columns=180 " perfect size for me
        set columns=90 " perfect size for me
        if mysys ==? "windows"
            let &guifont=join(values(font),':')
        else
            let &guifont=join(values(font))
        endif
        set guioptions=ec
        "              ||
        "              |+-- use simple dialogs rather than popups
        "              +-- use GUI tabs, not console style tabs
        " Note: I'm overriding the entire default guioptions string (i.e. not 
        " using += or -=). This means that the GUI menu is not displayed at all.
        set lines=55 " perfect size for me
        set mousehide " hide the mouse cursor when typing
        "set colorcolumn=80 " visibly mark the point where most code should end or wrap.
        "highlight colorcolumn ctermbg=322
    " }
    
    " Font Switching Binds {
        if mysys == "windows"
            map <F8> <ESC>:set guifont=Consolas:h8<CR>
            map <F8> <ESC>:set guifont=Consolas:h10<CR>
            map <F8> <ESC>:set guifont=Consolas:h12<CR>
            map <F8> <ESC>:set guifont=Consolas:h16<CR>
            map <F8> <ESC>:set guifont=Consolas:h20<CR>
        else
            map <F8> <ESC>:set guifont=Inconsolata\ 8<CR>
            map <F8> <ESC>:set guifont=Inconsolata\ 10<CR>
            map <F8> <ESC>:set guifont=Inconsolata\ 12<CR>
            map <F8> <ESC>:set guifont=Inconsolata\ 16<CR>
            map <F8> <ESC>:set guifont=Inconsolata\ 20<CR>
        endif
    " }
endif
" }


