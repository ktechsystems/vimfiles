" Guideline specific commands for cucumber files

" Commands (custom) {

  " Run a steps check
  command! -buffer Gsteps :! rake features:steps[%:p]
  " Pretty print in place
  command! -buffer Gppip :! rake features:ppip[%:p]
  " Run current file
  command! -buffer Gtest :! rake features[%:p]
  " Run current scenario
  command! -buffer -range Gscenario :! rake features[%:p:<line1>]

" }


