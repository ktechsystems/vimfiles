" RSpec specific commands for testing

" Commands (custom) {

  " Run current file
  command! -buffer Rtest :! rspec %:p
  " Run current file using bundler
  command! -buffer Rbtest :! bundle exec rspec %:p
  " Run current scenario
  command! -buffer -range Rscenario :! rspec %:p:<line1>
  " Run current scenario using bundler
  command! -buffer -range Rbscenario :! bundle exec rspec %:p:<line1>
  " Run current scenario using bundler and pry-rescue
  command! -buffer -range Rbrscenario :! bundle exec rescue rspec %:p:<line1>

" }


